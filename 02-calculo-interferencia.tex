\section{Cálculo da Interferência} \label{sec:calculo-interferencia}

O ambiente de testes utilizado neste trabalho consiste em séries de execuções
de aplicações preparadas para estressar diversos aspectos arquiteturais de
máquinas virtuais. O conjunto de aplicações utilizado foi o mesmo definido por
Koh \textit{et al.} \cite{koh2007}. Entre as aplicações escolhidas estão compilação de código
fonte, compressão e encriptação de arquivos e processamento de imagens, bem
como, ferramentas voltadas para geração de testes de \textit{benchmark} tais
como \textit{Cachebench}\footnote{\url{http://icl.cs.utk.edu/llcbench/cachebench.html}}
e \textit{AIM Benchmark Suite}\footnote{\url{ https://sourceforge.net/projects/aimbench}}.
São elas:
Add\_double\footnote{\url{https://www.gnu.org/software/m68hc11/examples/bench-float.html}},
Bzip2\footnote{\url{http://www.bzip.org}}, 
Gzip\footnote{\url{https://www.gzip.org}}, 
Ccrypt\footnote{\url{http://ccrypt.sourceforge.net}}, 
Cachebench, 
Cat\footnote{\url{https://www.gnu.org/software/coreutils/cat}},
Grep\footnote{\url{https://www.gnu.org/software/grep/}},
Cp\footnote{\url{https://www.gnu.org/software/coreutils/cp}},
Dd\footnote{\url{https://www.gnu.org/software/coreutils/dd}},
Iozone\footnote{\url{http://www.iozone.org}},
Make\footnote{\url{https://www.gnu.org/software/make}} e
Povray\footnote{\url{http://www.povray.org}}.


A fim de se ter comodidade na criação e destruição dessas
máquinas virtuais foi utilizado o \textit{OpenNebula}\footnote{\url{https://opennebula.org}}
como ferramenta de computação em nuvem e como \textit{hypervisor} foi utilizado o
\textit{KVM}\footnote{\url{https://www.linux-kvm.org/page/Main_Page}}. As
máquinas virtuais possuíam, como configuração, sistema operacional
\textit{Centos 7}\footnote{\url{https://www.centos.org}}, 
espaço em disco de 15GB e 1GB de memória \textit{RAM}. 
Em um primeiro momento as aplicações foram instaladas e testadas de modo que se
pudesse observar quais são os comandos utilizados para funcionamento das
mesmas, em seguida foram criados \textit{snapshots} das máquinas virtuais com o
auxílio provido pelo \textit{OpenNebula}.

Adotamos o mesmo procedimento proposto por Koh \textit{et al.}~\cite{koh2007} para o cálculo da
interferência: duas máquinas virtuais,  denominadas \textit{dom1} e
\textit{dom2} respectivament esão criadas em um servidor utilizando
\textit{KVM}. Cada máquina virtual executa uma das aplicações do
\textit{benchmarking}.

A aplicação executando em \textit{dom1} é chamada de aplicação
\textit{foreground}. A que executa em \textit{dom2} é chamada de aplicação
\textit{background}. Por questões de notação uma aplicação \textit{foreground}
executando contra uma aplicação \textit{background} é denotada como F@B. Um dos
procedimentos adotados é garantir que a aplicação \textit{background} mantenha
sua execução até que a aplicação \textit{foreground} termine. Cada aplicação é
executada de modo que seja tanto \textit{background} quanto
\textit{foreground}, sendo construída dessa forma uma matriz $n\times n$ com
todos as possíveis combinações de execução de duas aplicações. 

A fim de observar quanto o desempenho é afetado pela interferência gerada por
uma aplicação em execução em outra máquina virtual no mesmo servidor, é feita a
medida da degradação a partir do desempenho padrão de uma aplicação, essa
medida denomina-se \textbf{pontuação normalizada}. Para calcular a pontuação
normalizada de uma aplicação F contra uma aplicação B ($NS(F@B)$), divide-se a
pontuação de desempenho de F contra B ($PD(F@B)$) pela pontuação de desempenho
inativa de F, i.e., quando executada contra uma máquina virtual inativa
($PD(F@Inativo)$), ou seja, sem nenhuma aplicação executando. Em resumo:

\begin{equation}
\label{eq:degradation} 
	NS(F@B) = PD(F@B)/PD(F@Inativo)
\end{equation}

A partir da pontuação normalizada é feito cálculo do desempenho combinado de
duas aplicações, F e B, em cada máquina virtual:

\begin{equation}
\label{eq:combined} 
	NS ( F + B ) = NS ( F @ B ) + NS ( B @ F )
\end{equation}


\begin{table}[ht]
\centering
\caption{Aplicações utilizadas para geração de cargas e trabalho.}
\label{table-aplications} \resizebox{0.45\textwidth}{!}{
\begin{tabular}{|l|c|c|}
\hline Nome        & \multicolumn{1}{l|}{Maior Recurso
Utilizado} & \multicolumn{1}{l|}{Medida de Desempenho} \\ \hline
Add\_double & CPU                                          & Pontuação \\ \hline 
Bzip2       & Misto                                        & Tempo \\ \hline
Cat         & Disco                                        & Tempo \\ \hline
Cachebench  & Memória                                      &Pontuação \\ \hline
Ccrypt      & Misto 					   & Tempo    \\ \hline
Cp          & Disco					   & Tempo    \\ \hline
Dd          & Disco					   & Tempo    \\ \hline
Grep        & Disco					   & Tempo    \\ \hline
Gzip        & Misto					   & Tempo    \\ \hline
Iozone      & Disco					   & Pontuação \\ \hline
Make        & Misto					   & Tempo     \\ \hline
Povray      & Misto					   & Tempo     \\ \hline
\end{tabular}
}
\end{table}

Sendo NS(F@B) e NS(B@F) medidos em dois testes separados. Para medida de
desempenho são utilizadas as pontuações geradas pelas próprias aplicações.
Para aquelas que não são voltadas para \textit{benchmarking}, a pontuação de
desempenho é definida como o inverso do tempo necessário para sua execução. A
Tabela \ref{table-aplications} apresenta o maior recurso utilizado bem como a
medida de desempenho utilizada para cada ferramenta. Dessa forma, uma pontuação
normalizada de F contra B próxima ou igual a 1 indica que o desempenho de uma
aplicação F contra B sofreu baixa degradação.
